import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        File f = new File("test.txt");
        if (!f.exists()) {
            System.out.println("file does not exists");
            return;
        }

        LinkedList<String> list = new LinkedList() {
            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                Iterator iter = this.iterator();
                for (int i = 0; iter.hasNext(); i++) {
                    sb.append(i + ": " + iter.next() + "\n");
                }
                return sb.toString();
            }
        };

        try {
            Scanner scanner = new Scanner(f);
            while (scanner.hasNextLine()) {
                list.add(scanner.nextLine());
            }
            scanner.close();
        } catch (Exception e) {
            System.out.println("read error");
        }

        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
    }
}
